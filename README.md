# 99x technical assessment

Sample shopping cart API servier built using spring boot 2.0

## Assumptions
 - 10 % discount for the carton price and 30 % service charge addition for the single units will not show in the product/price list page (home page)
 - Those changes will be applied when the user requests a quotation.  

## Features

 - Swagger integrated - `/swagger-ui.html` for API documentation.
 - Test suite - JUnit5 and Mokito.
 - Code analysis - sonarQube.
 - Spring boot actuator enabled.

## Usage

*execute from the root of the project directory*

 - run with web application container - `mvn spring-boot:run`
 - run test suite - `mvn test`
 - build as a jar - `mvn clean package spring-boot:repackage`
 - execute the jar file - `java -jar target/generated-jar-file.jar`
 - enable sonarqube - follow the instruction [here.](https://docs.sonarqube.org/latest/setup/get-started-2-minutes/)

