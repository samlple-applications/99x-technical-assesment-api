package com.priceEngineService.service;

import com.priceEngineService.model.dao.ProductDAO;
import com.priceEngineService.model.dto.Quotation;
import com.priceEngineService.model.dto.UnitPrice;
import com.priceEngineService.model.repository.ProductRepository;
import com.priceEngineService.util.TestLifecycleLogger;
import com.priceEngineService.util.TimeExecutionLogger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PriceCalculationServiceImplTest implements TestLifecycleLogger, TimeExecutionLogger {

    @Mock
    ProductRepository productRepository;
    private PriceCalculationServiceImpl priceCalculationService;

    @BeforeEach
    public void setup() {
        priceCalculationService = new PriceCalculationServiceImpl(productRepository);
    }

    @Test
    void getPriceList() {
        ProductDAO product = new ProductDAO(1L, "Penguin-ears", 2, 10);
        Mockito.when(productRepository.findById(1L)).thenReturn(Optional.of(product));

        ArrayList<UnitPrice> prices = priceCalculationService.getPriceList(1l, 1, 3);

        assertEquals(prices.get(0).getPrice(), 5.0);
        assertFalse(prices.get(0).isCartonPrice());
        assertEquals(prices.get(0).getUnitNumber(), "1 - items");

        assertEquals(prices.get(1).getPrice(), 10.0);
        assertTrue(prices.get(1).isCartonPrice());
        assertEquals(prices.get(1).getUnitNumber(), "2 - items");

        assertEquals(prices.get(2).getPrice(), 15.0);
        assertFalse(prices.get(2).isCartonPrice());
        assertEquals(prices.get(2).getUnitNumber(), "3 - items");
    }

    @Test
    void calculatePrice() {
        ProductDAO pe = new ProductDAO(1L, "Penguin-ears", 20, 175);
        ProductDAO hs = new ProductDAO(1L, "Horseshoe", 5, 825);
        Mockito.when(productRepository.findById(1L)).thenReturn(Optional.of(pe));
        Mockito.when(productRepository.findById(2L)).thenReturn(Optional.of(hs));

        Map<Long, Integer> data = new HashMap<>();
        data.put(1L, 60);
        data.put(2L, 12);

        final Map<String, Quotation> quotationMap = priceCalculationService.calculatePrice(data);

        //checking optimized to price condition
        assertEquals(quotationMap.get("Penguin-ears").getNumberOfCartons(),3);
        assertEquals(quotationMap.get("Penguin-ears").getNumberOfSingleUnits(),0);
        assertEquals(quotationMap.get("Horseshoe").getNumberOfCartons(),2);
        assertEquals(quotationMap.get("Horseshoe").getNumberOfSingleUnits(),2);

        //10% discount for 3 or more cartoons
        assertEquals(quotationMap.get("Penguin-ears").getPriceOfCartons(),472.5); // with discount
        assertEquals(quotationMap.get("Horseshoe").getPriceOfCartons(),1650.0); //without discount

        //30% addition for single units
        assertEquals(quotationMap.get("Penguin-ears").getPriceOfSingleUnits(),0.0);
        assertEquals(quotationMap.get("Horseshoe").getPriceOfSingleUnits(),429.0); // 330 * 1.3

        //total quote
        assertEquals(quotationMap.get("Penguin-ears").getTotal(),472.5);
        assertEquals(quotationMap.get("Horseshoe").getTotal(),2079.0);

    }
}