package com.priceEngineService.controller;

import com.priceEngineService.model.dto.Quotation;
import com.priceEngineService.model.dto.UnitPrice;
import com.priceEngineService.service.PriceCalculationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

@RestController
@RequestMapping("/api/price")
public class PriceController {

    private final PriceCalculationService priceCalculationService;

    public PriceController(PriceCalculationService priceCalculationService){
        this.priceCalculationService = priceCalculationService;
    }

    /**
     *TODO: use pageable objects
     */
    @GetMapping("/list")
    public ResponseEntity<?> getPriceList(@RequestParam("productId") Long productId,
                                          @RequestParam("startPos") int startPos,
                                          @RequestParam("endPos") int endPos){
        ArrayList<UnitPrice> priceList = priceCalculationService.getPriceList(productId,startPos,endPos);
        return new ResponseEntity<>(priceList, HttpStatus.OK);
    }

    /**
     * map key - prod id
     * map value - quantity
     */
    @PostMapping("/calculate")
    public ResponseEntity<?> calculatePrice(@RequestBody Map<Long  ,Integer> request){
        Map<String, Quotation> quotationMap = priceCalculationService.calculatePrice(request);
        return new ResponseEntity<>(quotationMap,HttpStatus.OK);
    }
}
