package com.priceEngineService.model.dto;

public class Quotation {
    private int numberOfCartons;
    private double priceOfCartons;
    private int numberOfSingleUnits;
    private double priceOfSingleUnits;
    private double total;

    public void calculateTotal(int unitsPerCarton, double priceOfCarton) {

        this.priceOfCartons = this.numberOfCartons * priceOfCarton;

        if (this.numberOfCartons >= 3) {
            //10% discount for buying 3 or more units
            this.priceOfCartons = this.priceOfCartons - (this.priceOfCartons * 0.1);
        }

        //30% margin added for the single unit price
        this.priceOfSingleUnits = (this.numberOfSingleUnits * (priceOfCarton / (double) unitsPerCarton)) * 1.3;

        //rounding to two decimal points
        this.priceOfSingleUnits = (double) Math.round(this.priceOfSingleUnits * 100) / 100;
        this.total = (double) Math.round((this.priceOfCartons + this.priceOfSingleUnits) * 100) / 100;

    }

    public int getNumberOfCartons() {
        return numberOfCartons;
    }

    public void setNumberOfCartons(int numberOfCartons) {
        this.numberOfCartons = numberOfCartons;
    }

    public double getPriceOfCartons() {
        return priceOfCartons;
    }

    public void setPriceOfCartons(double priceOfCartons) {
        this.priceOfCartons = priceOfCartons;
    }

    public int getNumberOfSingleUnits() {
        return numberOfSingleUnits;
    }

    public void setNumberOfSingleUnits(int numberOfSingleUnits) {
        this.numberOfSingleUnits = numberOfSingleUnits;
    }

    public double getPriceOfSingleUnits() {
        return priceOfSingleUnits;
    }

    public void setPriceOfSingleUnits(double priceOfSingleUnits) {
        this.priceOfSingleUnits = priceOfSingleUnits;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
