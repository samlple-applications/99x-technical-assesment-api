package com.priceEngineService.model.dto;

public class UnitPrice {
    private String unitNumber;
    private double price;
    private boolean isCartonPrice = false;

    public boolean isCartonPrice() {
        return isCartonPrice;
    }

    public void setIsCartonPrice(boolean cartonPrice) {
        this.isCartonPrice = cartonPrice;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
