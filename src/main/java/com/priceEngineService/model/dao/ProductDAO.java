package com.priceEngineService.model.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ProductDAO {

    private @Id
    @GeneratedValue
    Long Id;
    private String name;
    private int unitsPerCarton;
    private double cartonPrice;

    public ProductDAO() {
    }


    public ProductDAO(String name, int unitsPerCarton, int cartonPrice) {
        this.name = name;
        this.unitsPerCarton = unitsPerCarton;
        this.cartonPrice = cartonPrice;
    }

    public ProductDAO(Long id, String name, int unitsPerCarton, double cartonPrice) {
        Id = id;
        this.name = name;
        this.unitsPerCarton = unitsPerCarton;
        this.cartonPrice = cartonPrice;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUnitsPerCarton() {
        return unitsPerCarton;
    }

    public void setUnitsPerCarton(int unitsPerCarton) {
        this.unitsPerCarton = unitsPerCarton;
    }

    public double getCartonPrice() {
        return cartonPrice;
    }

    public void setCartonPrice(int cartonPrice) {
        this.cartonPrice = cartonPrice;
    }

}
