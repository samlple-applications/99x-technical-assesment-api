package com.priceEngineService.model.repository;

import com.priceEngineService.model.dao.ProductDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ProductRepository extends JpaRepository<ProductDAO, Long> {
}
