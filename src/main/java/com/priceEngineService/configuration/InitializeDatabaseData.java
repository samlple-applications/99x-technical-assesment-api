package com.priceEngineService.configuration;

import com.priceEngineService.model.dao.ProductDAO;
import com.priceEngineService.model.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InitializeDatabaseData {

    private Logger logger = LoggerFactory.getLogger(InitializeDatabaseData.class);

    /**
     * TODO: run only when table is empty
     */
    @Bean
    CommandLineRunner populateDatabase(ProductRepository productRepository) {
        return args -> {
            logger.info("Populating database : initiated");
            productRepository.save(new ProductDAO("Penguin-ears", 20, 175));
            productRepository.save(new ProductDAO("Horseshoe", 5, 825));
            logger.info("Populating database : completed");
        };
    }
}
