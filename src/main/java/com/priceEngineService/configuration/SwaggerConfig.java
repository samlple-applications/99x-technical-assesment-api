package com.priceEngineService.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class SwaggerConfig {

    @Bean
    public Docket swaggerConfiguration() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("application")
                .select()
                .paths(PathSelectors.ant("/api/**"))
                .apis(RequestHandlerSelectors.basePackage("com.priceEngineService"))
                .build()
                .apiInfo(apiDetails());
    }

    @Bean
    public Docket swaggerAcuatorConfiguration() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("actuator")
                .select()
                .paths(PathSelectors.ant("/actuator/*"))
                .apis(RequestHandlerSelectors.any())
                .build();
    }

    private ApiInfo apiDetails() {
        return new ApiInfo(
                "Price Engine API",
                "Price engine sample implementation",
                "1.0",
                "Free to use",
                new Contact("Charith Jayawardana,", "https://cvljayawardana.wordpress.com/", "cvljayawardana@gmail.com"),
                "API License",
                "https://cvljayawardana.wordpress.com/",
                Collections.emptyList()
        );
    }
}
