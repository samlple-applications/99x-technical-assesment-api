package com.priceEngineService.service;

import com.priceEngineService.model.dao.ProductDAO;
import com.priceEngineService.model.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<ProductDAO> getAllProducts() {
        logger.info("Fetching products from database : initiated");
        List<ProductDAO> products = productRepository.findAll();
        logger.info("Fetching products from database : completed");
        return products;
    }
}
