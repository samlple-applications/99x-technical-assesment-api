package com.priceEngineService.service;

import com.priceEngineService.model.dto.Quotation;
import com.priceEngineService.model.dto.UnitPrice;

import java.util.ArrayList;
import java.util.Map;

public interface PriceCalculationService {

    ArrayList<UnitPrice> getPriceList(Long productId, int startPos, int endPos);
    Map<String, Quotation> calculatePrice(Map<Long, Integer> data);
}
