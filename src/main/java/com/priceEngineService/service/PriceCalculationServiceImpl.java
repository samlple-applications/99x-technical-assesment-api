package com.priceEngineService.service;

import com.priceEngineService.model.dao.ProductDAO;
import com.priceEngineService.model.dto.Quotation;
import com.priceEngineService.model.dto.UnitPrice;
import com.priceEngineService.model.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service
public class PriceCalculationServiceImpl implements PriceCalculationService {

    private final ProductRepository productRepository;
    private Logger logger = LoggerFactory.getLogger(PriceCalculationServiceImpl.class);

    @Autowired
    public PriceCalculationServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public ArrayList<UnitPrice> getPriceList(Long productId, int startPos, int endPos) {
        logger.info("generating price list : initiated");
        ProductDAO product = this.productRepository.findById(productId).get();
        ArrayList<UnitPrice> prices = generatePriceList(product, startPos, endPos);
        logger.info("generating price list : completed");

        return prices;
    }

    @Override
    public Map<String, Quotation> calculatePrice(Map<Long, Integer> data) {
        logger.info("calculating price : initiated");
        Iterator iterator = data.entrySet().iterator();

        Map<String, Quotation> quotationMap = new HashMap<>();
        Quotation quotation;
        while (iterator.hasNext()) {
            Map.Entry element = (Map.Entry) iterator.next();
            ProductDAO product = productRepository.findById((Long) element.getKey()).get();
            quotation = generateQuotation(product, (int) element.getValue());
            logger.info("calculating price : quotation generated for product id - {} | number of cartons - {} | number of single units - {} | price of cartons - {} | price of single units - {} | total price {}"
                    , product.getId(),quotation.getNumberOfCartons(),quotation.getNumberOfSingleUnits(),quotation.getPriceOfCartons(),quotation.getPriceOfSingleUnits(),quotation.getTotal());
            quotationMap.put(product.getName(), quotation);
        }

        logger.info("calculating price : completed");
        return quotationMap;
    }

    private ArrayList<UnitPrice> generatePriceList(ProductDAO product, int startPos, int endPos) {
        ArrayList<UnitPrice> priceList = new ArrayList<>();
        double price = 0;

        for (int i = startPos; i <= endPos; i++) {
            UnitPrice unit = new UnitPrice();
            if (i % product.getUnitsPerCarton() == 0) {
                price = (double) (i / product.getUnitsPerCarton()) * product.getCartonPrice();
                unit.setIsCartonPrice(true);
            } else {
                price = (price + (product.getCartonPrice() / (double) product.getUnitsPerCarton()));
            }
            unit.setUnitNumber(String.format("%s - items", i));
            price = (double) Math.round(price * 100) / 100;
            unit.setPrice(price);
            priceList.add(unit);
            logger.info("generating price list : calculating price for {} | price - {} | is carton price - {}", unit.getUnitNumber(), unit.getPrice(), unit.isCartonPrice());
        }
        return priceList;
    }

    private Quotation generateQuotation(ProductDAO product, int quantity) {
        Quotation quotation = new Quotation();

        int cartons = quantity / product.getUnitsPerCarton();
        int singleUnits = quantity % product.getUnitsPerCarton();
        quotation.setNumberOfCartons(cartons);
        quotation.setNumberOfSingleUnits(singleUnits);
        quotation.calculateTotal(product.getUnitsPerCarton(), product.getCartonPrice());

        return quotation;
    }
}
