package com.priceEngineService.service;

import com.priceEngineService.model.dao.ProductDAO;

import java.util.List;

public interface ProductService {

    List<ProductDAO> getAllProducts();
}
